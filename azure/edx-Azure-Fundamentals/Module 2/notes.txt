PuTTY is used to generate public and private keys

What is an Availability Set?
As an Azure administrator you must be prepared for planned (updates) and unplanned (hardware) failures. One way to prepare is with availability sets. An availability set is a logical grouping of two or more virtual machines.

When creating Availability sets keep these principles in mind.
For redundancy, configure multiple virtual machines in an Availability Set.
Configure each application tier into separate Availability Sets.
Combine a Load Balancer with Availability Sets.

Each virtual machine in an availability set is placed in one update domain and two fault domains.


What are Scale Sets?
Virtual machine scale sets are an Azure Compute resource you can use to deploy and manage a set of identical VMs. With all VMs configured the same, VM scale sets are designed to support true auto-scale � no pre-provisioning of VMs is required � and as such makes it easier to build large-scale services targeting big compute, big data, and containerized workloads.

What is Azure Resource Explorer?
Azure Resource Explorer is a great tool to view and modify resources you have created in your subscription. The tool is web-based and uses your Azure portal logon credentials. The source for the Resource Explorer tool is available on GitHub, which provides a valuable reference if you need to implement similar behavior in your own applications.