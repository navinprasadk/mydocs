
cf --version
cf login
cf login -a api.run.pivotal.io
cf target
cf push <name of the app> -> will push the entire folder into cloud
cf push <name of the app> -p xxx.jar/war -> push the particular file from that app folder
cf apps -> list the status of apps as stopped
cf stop
cf app <app name> -> showing the status of particular app
cf apps -> to access the app
cf restart <app name>
cf restage <app name>
cf delete <app name>
cf buildpacks


cf create-buildpack <name> <path> <position> -> crete buildpacks
cf update_buildpacks -> update/change the buildpack position

cf marketplace
cf marketplace -s cleardb
cf services -> list of services

cf create-service
cf create-service servicename plan instancename
cf bind-service apname instancename
cf env appname

cf scale appname -i 2

cf domains
cf routes

cf create-domain NavinOrg navin.io

cf map-route
cf unmap-route
cf delete-route

cf help -> list of commands
cf help login -> shows detailed help on login commands


PaperTrail:
cf cups LogDrainTest -l syslog-tls://logs2.papertrailapp.com:10299
cf bind-service GreenAppTest LogDrainTest